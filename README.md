# README #

### What is this repository for? ###

* TestTask
* Domru

### Description ###

* I had a little bit free time so just made a MVC structure of application, without filter, only one method and test for this method.
* Using Spring MVC this code more scalable and flexible.
* Maven as a popular and easy to use build manager
* Spring Boot for easier deployment
* FasterXML for converting enum to JSON wasn't implemented. It was is one of the most difficult problem in the project

### Thank you! ###