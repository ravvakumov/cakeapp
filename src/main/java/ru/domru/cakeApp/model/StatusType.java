package ru.domru.cakeApp.model;

public enum StatusType {
    FRESH,
    STALE
}
