package ru.domru.cakeApp.model;

public class Cake {
    int id;
    String name;
    StatusType status;

    public Cake(int id, String name, StatusType status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
