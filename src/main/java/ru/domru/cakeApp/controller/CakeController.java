package ru.domru.cakeApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.domru.cakeApp.model.Cake;
import ru.domru.cakeApp.service.CakeService;

@RestController
@RequestMapping("api/cakes")
public class CakeController {

    @Autowired
    CakeService cakeService;

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Cake getItem(@PathVariable("id") int id) {
        return cakeService.getItem(id);
    }
}
