package ru.domru.cakeApp.repository;

import org.springframework.stereotype.Repository;
import ru.domru.cakeApp.model.Cake;
import ru.domru.cakeApp.model.StatusType;

import java.util.Arrays;
import java.util.List;

@Repository
public class CakeRepository {

    private List<Cake> cakes = Arrays.asList(new Cake(1, "Cake1", StatusType.FRESH),
            new Cake(2, "Cake2", StatusType.STALE));

    public List<Cake> getAllItems() {
        return cakes;
    }
}
