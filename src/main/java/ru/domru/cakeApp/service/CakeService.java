package ru.domru.cakeApp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.domru.cakeApp.model.Cake;
import ru.domru.cakeApp.repository.CakeRepository;

import java.util.List;

@Service
public class CakeService {

    @Autowired
    CakeRepository cakeRepository;

    public Cake getItem(int id) {
        List<Cake> allItems =  cakeRepository.getAllItems();
        for(Cake cake: allItems) {
            if(cake.getId() == id)
                return cake;
        }
        return null;
    }
}
