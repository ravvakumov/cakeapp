package ru.domru.cakeApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CakeApp {
    public static void main(String[] args) {
        SpringApplication.run(CakeApp.class, args);
    }
}
