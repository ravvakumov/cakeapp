package ru.domru.cakeApp.service;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.domru.cakeApp.model.Cake;
import ru.domru.cakeApp.model.StatusType;

import static com.nitorcreations.Matchers.reflectEquals;

public class CakeServiceTest {

    @Ignore
    @Test
    public void testGetItem() {
        CakeService cakeService = new CakeService();
        int cakeId = 2;
        Cake expected = new Cake(cakeId, "Cake2", StatusType.STALE);
        Cake actual = cakeService.getItem(cakeId);//NullPointer
        Assert.assertThat(expected, reflectEquals(actual));
    }
}